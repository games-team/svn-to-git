#!/usr/bin/perl
# Copyright 2008 Gonéri Le Bouder <goneri@rulezlan.org>
# MIT licence.
#
# Usage:
# cd /somewhere/pkg-games/packages/trunk
# ./mkGitSvnAliasFile.pl > alias

use strict;
use warnings;

use File::Find;

my %hash = (
  'baby-guest' => 'Miriam Ruiz <little_miry@yahoo.es>',
  'eddyp-guest' => 'Eddy Petrișor <eddy.petrisor@gmail.com>',
  'tolimar' => 'Alexander Reichle-Schmehl <tolimar@debian.org>'
);

sub wanted () {
    return unless -f $File::Find::name;
    return unless $File::Find::name =~ /\/debian\/changelog/;

    open CHANGELOG, "<$File::Find::name" or die "$!";
    foreach (<CHANGELOG>) {
        next unless /^ -- (\w.+<(\S+)@[\S]+>)  /;
        my $login = $2;
        my $who = $1;
        next if exists($hash{$login}) && $hash{$login} =~ /debian\.org/;
        $hash{$login} = $who;
    }
    close CHANGELOG;
}

finddepth({ wanted => \&wanted, follow => 1, no_chdir => 1 }, '.');
foreach (keys %hash) {
  print $_." = ".$hash{$_}."\n";
  print $_."-guest = ".$hash{$_}."\n";
}
